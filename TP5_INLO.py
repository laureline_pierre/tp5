#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Lauréline Pierre"

"""Pour la liste chainée, j'ai repris celle de Marina. J'étais absente lors du derneir TP et je n'ai pas eu le temps de le rattraper"""

import unittest
from chained_list_mg import ChainedList, Node

class TestLinkedList(unittest.TestCase):

    def setUp(self):
        """Initialization of tests. Define a variable that we can reuse"""
        self.test_list = ChainedList()
        self.first_node = None


    def test_isEmpty(self):
        """Verify if the created linked list is empty"""
        self.assertIsNone(self.test_list.first_node)

    def test_AddNode(self):
        """Verify if a linked list with elements is not empty"""
        self.test_list.add_node(9)
        self.test_list.add_node(10)
        self.test_list.add_node(11)
        self.assertIsNotNone(self.test_list)

    def test_InchangedList(self):
        """Verify if a linked list in which the same element was add and remove is inchanged"""
        init_list = []
        final_list = []
        self.test_list.add_node(9)
        self.test_list.add_node(10)
        self.test_list.add_node(11)
        self.test_list.add_node(13)
        for value in self.test_list:
            i = value.value
            init_list.append(i)
        self.test_list.add_node(12)
        self.test_list.delete_node(12)
        for value in self.test_list :
            j = value.value
            final_list.append(j)
        self.assertEqual(init_list,final_list)

    def test_PicList(self):
        """Verify if the last element added is at the end of the linked list"""
        the_list= []
        self.test_list.add_node(9)
        self.test_list.add_node(10)
        self.test_list.add_node(11)
        for value in self.test_list:
            i = value.value
            the_list.append(i)
        l = len(the_list)
        idx = l-1
        self.assertEqual(the_list[idx], 11)



